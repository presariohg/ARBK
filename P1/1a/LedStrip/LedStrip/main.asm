;
; LedStrip.asm
;
; Created: 26/10/2020 09:20:24
; Author : Tam Nguyen
;


; Replace with your application code
.include "m328pdef.inc"

.def temp       = r16
.def ledstatus  = r17
.def counter    = r18
.def go_left    = r19

; consts
.equ LIMIT      = 0x8
.equ TRUE       = 0x1

.org 0x0000
    rjmp        main

.org OVF0addr ;hardcoded in m328pdef.inc
    rjmp        timer0_overflow

main: 
    ;init stack pointer
    ldi     temp, HIGH(RAMEND)
    out     SPH,  temp
    ldi     temp, LOW(RAMEND)
    out     SPL, temp

    rcall   init_app            ; init app (constants, etc)
    rcall   init_timer          ; init timer (takt, mode, interrupts)

main_loop:
    rjmp    main_loop

init_app:
    ; all bits in port D set to OUT
    ldi     temp, 0b1111_1111
    out     DDRD, temp

    ldi     ledstatus, 0b0000_0001  ; start on the right most
    ldi     counter, 0x00           ; reset timer to 0
    ldi     go_left, 1              ; at first, start at right most and go left

    ret

init_timer:
    cli                             ; block all interrupts
    in      temp, TCCR0B            ; load TCCR0B to temp
    cbr     temp, 0b0000_0111       ; clear 3 first bits, reset all freq settings
    sbr     temp, 0b0000_0101       ; set bit 0 & 2 to high => freq = prescaler/1024 (lowest possible)
    out     TCCR0B, temp            ; write back down to TCCR0B

    ldi     temp, 0x00              ; set timer counter to 0
    out     TCNT0, temp
    ldi     temp, (1<<TOIE0)        ; enable Timer Overflow Interrupt 0
    STS     TIMSK0, temp

    sei                             ; unblock all interrupts 
    ret

timer0_overflow:
    inc     counter                 ; increase counter with each overflow..
    cpi     counter, LIMIT   
    brlo    endSub                  ; ...but reti if counter < a certain value (can be changed to control blinking freq)

    ; else waited enough,
    out     PORTD, ledstatus        ; write ledstatus to PORTD
    ldi     counter, 0x00           ; reset counter back to 0

    cpi     go_left, TRUE           ; check if is going to the left
    breq    left_shift              ; true: call left_shift()
    brne    right_shift             ; false: call right_shift()

left_shift:
    lsl     ledstatus               ; shift the led to the left
    cpi     ledstatus, 0b1000_0000
    brne    endSub                  ; if is shifting left and not reached left most yet, do nothing

    ldi     go_left, 0              ; else reached left most, change the direction

    reti                            

right_shift:
    lsr     ledstatus               ; shift the led to the right
    cpi     ledstatus, 0b0000_0001  
    brne    endSub                  ; if is shifting right and not reached right most yet, do nothing

    ldi     go_left, 1              ; else reached right most, change the direction

    reti

endSub:
    reti