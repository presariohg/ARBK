;
; LedStrip.asm
;
; Created: 26/10/2020 09:20:24
; Author : Tam Nguyen
;


; Replace with your application code
.include "m328pdef.inc"

.def temp       = r16
.def ledstatus  = r17
.def go_left    = r18

; consts
.equ LIMIT      = 0xff
.equ TRUE       = 0x1

.org 0x0000
    rjmp        main

main: 
    ;init stack pointer
    ldi     temp, HIGH(RAMEND)
    out     SPH,  temp
    ldi     temp, LOW(RAMEND)
    out     SPL, temp

    rcall   init_app            ; init app (constants, etc)
    rcall   main_loop

main_loop:
    out     PORTD, ledstatus
    cpi     go_left, TRUE           ; check if is going to the left
    breq    left_shift              ; true: call left_shift()
    brne    right_shift             ; false: call right_shift()


delay_one_fifth:
    ldi  r19, 17
    ldi  r20, 60
    ldi  r21, 204

    rcall   L1
    ret

L1: dec  r21                    ; loop value, 0x00->0xff = 255 cycles
    brne L1                     ; == 0 => do nothing, else not reached 0 yet, continue decreasing r21
    dec  r20                    ; loop value, 0x00->0xff = 255*255 cycles
    brne L1                     ; == 0 => do nothing, else not reached 0 yet, repeat loop
    dec  r19                    ; loop value, 0 -> 17 = 255*255*17 cycles
    brne L1                     ; == 0 => break loop, else not reached 0 yet, repeat loop
    ret
    
init_app:
    ; all bits in port D set to OUT
    ldi     temp, 0b1111_1111
    out     DDRD, temp

    ldi     ledstatus, 0b0000_0001  ; start on the right most
    ldi     go_left, 1              ; at first, start at right most and go left

    ret

left_shift:
    lsl     ledstatus               ; shift the led to the left
    cpi     ledstatus, 0b1000_0000

    brne    endSub                  ; if is shifting left and not reached left most yet, do nothing

    ldi     go_left, 0              ; else reached left most, change the direction

    rjmp    endSub

right_shift:
    lsr     ledstatus               ; shift the led to the right
    cpi     ledstatus, 0b0000_0001  
    brne    endSub                  ; if is shifting right and not reached right most yet, do nothing

    ldi     go_left, 1              ; else reached right most, change the direction

    rjmp    endSub

endSub:
    rcall   delay_one_fifth
    rjmp    main_loop