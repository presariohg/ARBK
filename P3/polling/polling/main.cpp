/*
 * pooling.cpp
 *
 * Created: 07/12/2020 00:54:32
 * Author : Tam Nguyen
 */ 
#define F_CPU 16000000
#include <util/delay.h>
#define ON 1
#define OFF 0

#define bit_toggle(Port, Bit) (Port ^= (1 << Bit))
#define bit_set(Port, Bit) (Port |= (1 << Bit))
#define bit_clear(Port, Bit) (Port &= ~(1 << Bit))
#define shift_left(Port) (Port <<= 1)
#define shift_right(Port) (Port >>= 1)
#define bit_read(Port, Bit) ((Port & (1<<Bit)) != 0)


#include <avr/io.h>
/**
 * Representing a single bit of a port. Use this instead of raw bitwise operations to improve readability
 */
class PortBit {
public:
    volatile uint8_t * port; // each PORT variable represents its physical counterpart, so it must be passed by reference
    int bit;
    PortBit(volatile uint8_t * port, int bit) {
        this->port = port;
        this->bit = bit;
    }

    /**
     * Flip the state of this bit.
     */
    void toggle() {
        bit_toggle(*this->port, this->bit);
    }

    /**
     * Only use when this bit is set to input. Using on an output bit will result in an error   
     * @return True if this button is being pressed, otherwise false
     */
    bool is_pressed() {
        return bit_read(*this->port, this->bit) == OFF; // button pressed => voltage = 0
    }

    /**
     * Set this bit's value.
     * Only use when this bit is set to output. Using on an input bit will also result in an error   
     * @param The value to set to
     */
    void set(int value = 1) {
        if (value == 1) 
            bit_set(*this->port, this->bit);
        else 
            bit_clear(*this->port, this->bit);
    }
};

void ports_init() {
    // set port d bit 0 & 1 to out to control LEDs
    bit_set(DDRD, 0);
    bit_set(DDRD, 1);

    // set start state to off
    bit_clear(PORTD, 0);
    bit_clear(PORTD, 1);

    // set port b bit 0 & 1 to in to receive button inputs
    bit_clear(DDRB, 0);
    bit_clear(DDRB, 1);

    // pull up bit 0 & 1
    bit_set(PORTB, 0);
    bit_set(PORTB, 1);
}

int main(void) {
    ports_init();
    
    PortBit sw1 = PortBit(&PINB, 0);
    PortBit sw2 = PortBit(&PINB, 1);
    
    PortBit led_d0 = PortBit(&PORTD, 0);
    PortBit led_d9 = PortBit(&PORTD, 1);
    PortBit blinking_target = led_d0;

    // volatile uint8_t * portd = &PORTD;
    
    bool is_blinking = true;
    
    while (1) {
        // button 2 pressed, switch state into blinking led d9
        if (sw2.is_pressed()) {
            while (sw2.is_pressed());
            
            if (!is_blinking) {
                blinking_target = led_d9;
                is_blinking = true;
                continue;
            }
            
            if (blinking_target.bit == led_d9.bit) {
                is_blinking = false;
                led_d9.set(OFF);
            } else {
                blinking_target = led_d9;
                led_d0.set(OFF);    
            }
            
        }

        // button 1 pressed, switch state into blinking led d0
        if (sw1.is_pressed()) {
            while (sw1.is_pressed());
            
            if (!is_blinking) {
                blinking_target = led_d0;
                is_blinking = true;
                continue;
            }
            
            if (blinking_target.bit == led_d0.bit) {
                is_blinking = false;
                led_d0.set(OFF);
            } else {
                blinking_target = led_d0;
                led_d9.set(OFF);
            }

        }
        
        if (is_blinking)
            blinking_target.toggle();
            
        _delay_ms(500);
    }
}

