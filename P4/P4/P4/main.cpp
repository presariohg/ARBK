/*
 * P4.cpp
 *
 * Created: 02/01/2021 12:41:08
 * Author : Tam Nguyen
 */ 

#define F_CPU 16000000

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "portbit.h"

volatile uint_fast32_t timer_counter_ms = 0;
int timer_count = 0;

const int TIMER_LIMIT_VALUE = 0xFFFF;
ISR(TIMER0_OVF_vect) {
    // Reinitialize Timer 0 value
    TCNT0=0x06;

    timer_counter_ms++;
    if (timer_counter_ms >= TIMER_LIMIT_VALUE) { // 0xFFFF = max uint32 value
        timer_counter_ms = 0;
    }
}


void wait_for(uint32_t time_ms) {
    uint32_t timestamp_start = timer_counter_ms;
    uint64_t timestamp_stop = timestamp_start + time_ms;

    while (timer_counter_ms < timestamp_stop) {
        if (timestamp_stop > TIMER_LIMIT_VALUE) {
            if (timer_counter_ms > timestamp_start)
                //if timer counter not reached data type's max value, do nothing
                continue;   
            else 
                // else timer counter reached data type's max value and was reseted to 0
                // => also reduce the stop timestamp
                timestamp_stop -= TIMER_LIMIT_VALUE;
        }
    }
}


void wait_until(uint32_t timestamp_ms) {
    while (timer_counter_ms < timestamp_ms);
}


void timer0_init() {
    cli();
    port_clear(TCCR0A, 0b11110011); //clear all bits except 2 & 3
    TCCR0B = (0<<WGM02) | (0<<CS02) | (1<<CS01) | (1<<CS00); // set prescaler clk/64
    TCNT0 = 0x06; // timer counter start value
    OCR0A = 0x00;
    OCR0B = 0x00;
    
    TIMSK0 = (1 << TOIE0); //enable timer 0 overflow interrupt
    sei();
}

PortBit led_d0 = PortBit(&PORTD, 0);
int main(void) {
    timer0_init();
    // set port D bit 0 & 1 to out to control LEDs
    bit_set(DDRD, 0);

    // set LEDs start state to off
    led_d0.set(OFF);

    wait_until(3000);

    /* Replace with your application code */
    while (1) {
        wait_for(500);
        led_d0.toggle();
    }
}

