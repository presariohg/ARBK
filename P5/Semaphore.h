//
// Created by presariohg on 03/01/2021.
//

#ifndef P5_SEMAPHORE_H
#define P5_SEMAPHORE_H
#include <mutex>

class Semaphore {
public:
    explicit Semaphore(uint32_t i) : count(i){};
    void acquire() {
        if (count > 0)
            count--;
        if (count <= 0)
            mutex.lock();
    }

    void release() {
        count++;
        if (count > 0) {
            mutex.unlock();
        }
    }

private:
    int count;
    std::mutex mutex;
};


#endif //P5_SEMAPHORE_H
