#define bit_toggle(Port, Bit) (Port ^= (1 << Bit))
#define bit_set(Port, Bit) (Port |= (1 << Bit))
#define bit_clear(Port, Bit) (Port &= ~(1 << Bit))
#define bit_read(Port, Bit) ((Port & (1<<Bit)) != 0)

#define shift_left(Port) (Port <<= 1)
#define shift_right(Port) (Port >>= 1)

#define port_set(Port, Mask) (Port |= Mask)
#define port_clear(Port, Mask) (Port &= ~Mask)
#define port_toggle(Port, Mask) (Port ^= Mask)

#define ON 1
#define OFF 0

class PortBit {
public:
    volatile uint8_t * port; // each PORT variable represents its physical counterpart, so it must be passed by reference
    int bit;
    PortBit(volatile uint8_t * port, int bit) {
        this->port = port;
        this->bit = bit;
    }

    /**
     * Flip the state of this bit.
     */
    void toggle() {
        bit_toggle(*this->port, this->bit);
    }

    /**
     * Only use when this bit is set to input. Using on an output bit will result in an error   
     * @return True if this button is being pressed, otherwise false
     */
    bool is_pressed() {
        return bit_read(*this->port, this->bit) == OFF; // button pressed => voltage = 0
    }

    /**
     * Set this bit's value.
     * Only use when this bit is set to output. Using on an input bit will also result in an error   
     * @param The value to set to
     */
    void set(int value = 1) {
        if (value == 1) 
            bit_set(*this->port, this->bit);
        else 
            bit_clear(*this->port, this->bit);
    }
};

